use aplicacion1;

CREATE TABLE noticias (
    id int not null auto_increment, titulo varchar(255) not null, texto varchar(255) not null, PRIMARY KEY (id)
);

INSERT INTO noticias (id, titulo, texto) VALUES (1, 'Noticia 1', 'Texto de la noticia 1'), (2, 'Noticia 2', 'Texto de la noticia 2'), (3, 'Noticia 3', 'Texto de la noticia 3')