<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PrincipalController extends Controller
{
    public function index()
    {
        //logica de control
        
        return view('principal.menu');
    }


    //accion para mostrar la vista de noticias
    public function noticias()
    {
        //logica de control
        //sacamos las noticias de la base de datos
        $noticias=DB::table('noticias')->get();
        return view('noticias',[
            'noticias' => $noticias
        ]);
    }
}
