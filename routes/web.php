<?php

use App\Http\Controllers\PrincipalController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

//ruta de tipo get para /mensaje
//va a la vista mensaje
Route::get('/mensaje', function () {
    return "hola clase";
});

//ruta de tipo get para /menu
Route::get('/menu', function () {
    $menu = "<ul>
    <li>item 1</li>
    <li>item 2</li>
    <li>item 3</li>
    </ul>";
    return $menu;
});


//rutas de tipo get que llama a una vista
//cuando escriba /inicio me renderiza la vista inicio
Route::get('/inicio', function () {
    //aqui puedo poner logica de control
    return view('inicio');
});

//ruta para carga una vista
//carga directamente la vista sin posibilidad de logica de control
Route::view('/contacto', 'contacto');

//ruta tipo get para /listado
//donde me muestramlos dias de la semana
Route::get('/listado', function () {
    return view('listado', ['dias' => [
        'lunes',
        'martes',
        'miercoles',
        'jueves',
        'viernes',
        'sabado',
        'domingo'
    ]]);
});

//ruta de tipo view para renderizar la vista colores
Route::view('/colores', 'colores', [
    'colores' => ['red', 'blue', 'green','yellow'
    ]
]);

//ruta de tipo get para renderizar la vista menu1
//llamar a la accion index del controlador principal y renderiza la vista menu1
//llama a la raiz
Route::get('/', [PrincipalController::class, 'index']);


//ruta de tipo get para listar las noticias de la tabla
Route::get('/noticias', function(){
    //no tenemos modelo lo hacemos con la clase DB
    //hace select * from noticias
    $noticias=DB::table('noticias')->get();
    return $noticias;
});

//ruta de tipo get
// /noticias1==> LLamar a la vista noticias en la vista noticias me muestra todas las noticias 
//               de la tabla noticias

Route::get('/noticias1', function(){
    //hacemos la consulta de la base de datos
    $noticias = DB::table('noticias')->get();
    return view('noticias',[
        'noticias' => $noticias
    ]);
});

//ruta de tipo get
// /noticias2==> LLamar a la accion noticias del controlador principal en la accion noticias y
//               saco todas las noticias de la tabla y llama a la vista noticias

Route::get('/noticias2', [PrincipalController::class, 'noticias']);